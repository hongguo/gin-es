package routers

import (
	"Gin-ES/app/controllers/home/v1"
	"Gin-ES/app/controllers/home/v1/login"
	"github.com/gin-gonic/gin"
)

func InitRouter() *gin.Engine {
	router := gin.Default()

	router.Use(gin.Logger()) //启动日志中间件
	//防止panic发生，返回500
	router.Use(gin.Recovery()) //启动Recovery中间件


	//创建路由组apiv1
	apiv1 := router.Group("/api/v1")
	apiv1.GET("/login",login.Login)


	//不参与apiv1路由组
	router.GET("/", v1.Hello)

	return router
}