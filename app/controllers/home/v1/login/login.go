package login

import (
	"Gin-ES/app/utils/responses"
	"Gin-ES/constants"
	"github.com/gin-gonic/gin"
)

// Login 登陆
func Login(ctx *gin.Context)  {
	response := responses.Gin{C: ctx}
	response.ResponseOk(constants.ErrCodes.ErrNo,nil)
}