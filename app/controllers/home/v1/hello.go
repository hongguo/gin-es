package v1

import (
	"github.com/gin-gonic/gin"
	http "net/http"
)

func Hello(ctx *gin.Context)  {
	ctx.String(http.StatusOK,"hello word!")
}
