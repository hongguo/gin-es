package responses

import (
	"Gin-ES/constants"
	"github.com/gin-gonic/gin"
)

type Gin struct {
	C *gin.Context
}

type Response struct {
	Success bool        `json:"success"`
	Code    int         `json:"code"`
	Msg     string      `json:"msg"`
	Data    interface{} `json:"data"`
}

// ResponseOk 请求成功响应
func (g *Gin) ResponseOk(errCode constants.ErrCode, data interface{}) {
	g.C.JSON(errCode.HTTPCode, Response{
		Success: true,
		Code: errCode.Code,
		Msg:  errCode.Desc,
		Data: data,
	})
}

func (g *Gin) ResponseErr(errCode constants.ErrCode) {
	g.C.JSON(errCode.Code, Response{
		Success: false,
		Code:    errCode.Code,
		Msg:     errCode.Desc,
		Data:    nil,
	})
}
